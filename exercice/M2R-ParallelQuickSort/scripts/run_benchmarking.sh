OUTPUT_DIRECTORY=data/`hostname`_`date +%F`
mkdir -p $OUTPUT_DIRECTORY
OUTPUT_FILE=$OUTPUT_DIRECTORY/measurements_`date +%R`.txt

touch $OUTPUT_FILE
for i in 100 1000 10000 100000 200000 300000 400000 500000 600000 700000 800000 900000 1000000 3000000; do
    for rep in `seq 1 5`; do
        echo "Size: $i" >> $OUTPUT_FILE;
        ./src/parallelQuicksort $i >> $OUTPUT_FILE;
    done ;
done

perl scripts/csv_quicksort_extractor.pl < $OUTPUT_FILE > ${OUTPUT_FILE}.csv
cp ${OUTPUT_FILE}.csv data/latest_data.csv
